<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 27/10/2018
 * Time: 16:17
 */

namespace App\Service;

use App\Entity\Media;
use Doctrine\Common\Persistence\ManagerRegistry;

class MediaService
{

    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry) {
        $this->managerRegistry = $managerRegistry;
    }

    protected function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }



    public function getMedia(): array
    {

        $entityManager = $this->getManagerRegistry()->getManager();
        $media = $entityManager->getRepository(Media::class)->findAll();
        return $media;

    }

    public function getMediaById($id): ?Media
    {

        $entityManager = $this->getManagerRegistry()->getManager();
        $media = $entityManager->getRepository(Media::class)->find($id);

        if ($media instanceof Media)
            return $media;
        else
            return null;

    }

    public function getMediaByString($string): array
    {

        $entityManager = $this->getManagerRegistry()->getManager();
        $videos = $entityManager->getRepository(Media::class)->findAll();
        $videosFiltrados = [];
        foreach ($videos as $media) {
            if ($media instanceof Media && strpos(strtolower($media->getTitle()), strtolower($string)) !== false) {
                $videosFiltrados[] = $media;
            }
        }

        return $videosFiltrados;

    }

}