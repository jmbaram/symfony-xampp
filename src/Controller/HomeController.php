<?php
/**
 * Created by PhpStorm.
 * User: JBR
 * Date: 27/10/2018
 * Time: 15:00
 */

namespace App\Controller;

use App\Service\MediaService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{

    /**
     * @Route("/")
     */
    public function home () {

        return $this->render('home.html.twig');

    }

    /**
     * @Route("/yt", name="Lista YouTube")
     * @param MediaService $mediaService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function yt (MediaService $mediaService) {

        return $this->render('list.html.twig',
            ['string' => 'YT List', 'yt_entries' => $mediaService->getMedia()]
        );

    }

    /**
     * @Route("/yt/{string}", name="YouTube video {string}")
     * @param MediaService $mediaService
     * @param string $string
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ytByString (MediaService $mediaService, string $string) {

        $media = $mediaService->getMediaByString($string);

        if (!empty($media))
            return $this->render('list.html.twig',
                ['string' => $string, 'yt_entries' => $media]
            );
        else {
            return new Response('<html><body><h2>Mala suerte tu</h2></body></html>');
        }
    }

}